module AcquireFile (acquireFile) where

import qualified System.IO.Strict as S
import System.IO
import System.IO.Error
import System.Directory
import System.Exit
import System.Process
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy.Char8 as BL
import Control.Exception
import Control.Monad
import Data.List

-- File acquirance

acquireFile :: String -> Bool -> IO B.ByteString
acquireFile url offline = do
      cachePath <- chooseCachePath
      case cachePath of
        Nothing -> do
                    hPutStrLn stderr "chooseCachePath could not find a cache path"
                    exitFailure
        Just path -> do
                   createDirectoryIfMissing False path
                   let savename = path ++ "/"++ map fixChar url
                   let tmpname = savename ++ ".tmp"
                   ex <- doesFileExist savename
                   when (offline && not ex) $ do
                      hPutStrLn stderr $ "Cached file for " ++ url ++ " does not exist, cannot use offline mode."
                      exitFailure
                   let args = [ "-f", "-R" , "-s", "-S", "-L", "-o", savename] ++
                              (if ex then ["-z", savename] else []) ++
                              [ url ]
                   unless offline $ do
                     readProcess "/usr/bin/curl" args ""
                     ex <- doesFileExist tmpname
                     when ex $ renameFile tmpname savename
                     return ()
                   ex <- doesFileExist savename
                   unless ex $ do
                        hPutStrLn stderr $ "File " ++ savename ++ " does not exist after invoking"
                        hPutStrLn stderr $ intercalate " " ("curl" : args)
                        exitFailure
                   B.readFile savename
    where fixChar '/' = '_'
          fixChar ':' = '_'
          fixChar c   = c

chooseCachePath :: IO (Maybe String)
chooseCachePath = do
  result <- tryJust shouldCatch $ getAppUserDataDirectory "reasons"
  hasHome <- getHomeDirectory >>= doesDirectoryExist
  return $ case result of
             Right dir -> if hasHome
                          then Just dir
                          else Nothing
             Left _ -> Nothing
      where shouldCatch e = if isDoesNotExistError e
                            then Just e
                            else Nothing

