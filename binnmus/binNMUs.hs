-- bin-NMU calculator
-- Copyright © 2014-2018 Joachim Breitner et al.
-- This software can be copied under the terms of the Expat license
-- (https://spdx.org/licenses/MIT)

{-# LANGUAGE DeriveGeneric #-}

import Text.Printf
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Char
import qualified Codec.Compression.GZip
import qualified Codec.Compression.Lzma
import Data.Ord
import Data.Function
import Options.Applicative
import Options.Applicative.Types (readerAsk)
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import Debian.Control.ByteString
import Debian.Relation.Common
import Debian.Relation.ByteString
import Control.Monad
import Text.Regex.PCRE
import System.IO
import System.Exit
import Control.Arrow
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy as BL
import Data.Time
import Data.Monoid ((<>))
import Control.Lens hiding (argument)
import Control.Seq
import GHC.Generics (Generic)
import Control.DeepSeq
import Data.Foldable (for_)
import qualified Data.Aeson as A
import Data.Aeson.Encode.Pretty (encodePretty)


import Database.PostgreSQL.Simple hiding (Binary)
import Database.PostgreSQL.Simple.Types (Query(Query))

import AcquireFile

defaultArches :: [Arch]
defaultArches = words "amd64 arm64 armel armhf i386 mips64el ppc64el riscv64 s390x"

-- non-default arches
extraArches :: [Arch]
extraArches = words "sparc"

portsArches :: [Arch]
portsArches = words "alpha arc hppa hurd-i386 ia64 loong64 m68k powerpc ppc64 sh4 sparc64 x32"

allArches :: [Arch]
allArches = defaultArches ++ extraArches ++ portsArches

type SourceName = String
type Version = String
type Arch = String

data VirtPackage = VirtPackage
    { vpFull   :: String
    , vpBase   :: VirtPackageBase
    , vpHashes :: [String]
    }
    deriving (Eq, Ord, Show, Generic)
instance NFData VirtPackage
type VirtPackageBase = String -- just the part without the hash (and without libghc-)

data Status -- ^ Status of a binNMU
    = Needed -- ^ This binNMU schould be scheduled
    | Waiting -- ^ This binMU is already scheduled, but in state BD-Uninstallable
    | Failed -- ^ This package is in a bad state in wanna-build
    | DepsMissing -- ^ Some dependencies are not present at all (NEW?)
    | WrongSrcVersion -- ^ Different source versions (out-of-date)
    | WrongVersion -- ^ Wrong version in wanna build
    | PendingRemoval -- ^ Removal pending
    | Missing -- ^ Doesn't exist in wanna build?
  deriving (Eq, Ord, Show)

data Reason
    = MissingDep VirtPackage
    | UpgradedDep String String String
  deriving (Eq, Ord, Show)

type BinNMU = (Binary, [Reason])
type CBinNMU = (Status, ((SourceName, Version), Arch, [Reason]))

data JBinNMU = JBinNMU
    { _pkg :: String
    , _ver :: String
    , _arches :: [String]
    , _suite :: String
    , _reason :: String
    } deriving Generic

customOptions = A.defaultOptions
                { A.fieldLabelModifier = drop 1
                }

instance A.ToJSON JBinNMU where
    toJSON     = A.genericToJSON customOptions
    toEncoding = A.genericToEncoding customOptions

data Binary = Binary
    { bPkgName :: String
    , bSourceName :: SourceName
    , bArchitecture :: Arch
    , bVersion :: Version
    , bSrcVersion :: Version
    , bDepends :: [VirtPackage] --simplified, we consider relevant deps only
    , bProvides :: [VirtPackage]
    }
  deriving (Show, Generic)
instance NFData Binary

-- The main action

run :: Conf -> IO ()
run conf = case outputFormat conf of
    WannaBuild -> do
        printHeader conf
        when (not (sql conf) && distribution conf /= "sid") $ do
            putCLn $ "When reading data via HTTP, only sid is supported"
            putCLn $ "as other distributions do not have dumps of the wanna-build data."
            putCLn $ "Use --sql on wuiet.debian.org!"
            exitFailure
        rms <- fetchWnppDump conf
        cBinNMUss <- mapM (getNMUs conf rms) (arches conf)
        -- Parallelization, if required
        --let cBinNMUs = concat (cBinNMUss `using` parList (evalList (evalTuple2 rseq rseq)))
        let cBinNMUs = concat cBinNMUss
        doPresentProblems conf cBinNMUs
        presentBinNMUs conf cBinNMUs
    JSON -> do
        when (not (sql conf) && distribution conf /= "sid") $ do
            exitFailure
        rms <- fetchWnppDump conf
        cBinNMUss <- mapM (getNMUs conf rms) (arches conf)
        let cBinNMUs = concat cBinNMUss
            json = encodePretty (finalizeBinNMUs conf cBinNMUs)
        BL.putStr $ json

printHeader :: Conf -> IO ()
printHeader conf = do
    putCLn $ "This is Debian Haskell Group's binNMU script. Send questions to <debian-haskell@lists.debian.org>"
    putCLn $ "My source is in https://salsa.debian.org/haskell-team/tools"
    t <- getZonedTime
    putCLn $ "It is now " ++ show t
    putCLn $ "I am looking at " ++ distribution conf
    putCLn $ "I am processing these architectures: " ++ intercalate ", " (arches conf)
    putCLn $ "I am looking for virtual packages matching " ++ regexS conf
    putCLn $ if sql conf then "I read my data from SQL."
                         else "I read my data via HTTP."
    putStrLn ""

-- | Presentation of binNMUs

doPresentProblems :: Conf -> [CBinNMU] -> IO ()
doPresentProblems conf cBinNMUs
    | not (presentProblems conf) = do
        return ()
    | S.null problems  = do
        putCLn "No problems detected"
    | otherwise = do
        putCLn "These dependency changes are observed:"
        mapM_ (putCLn . formatReason) (S.toAscList problems)
        putStrLn ""
  where problems =
            S.fromList $
            concatMap (^. (_2._3)) $
            filter (not . ignoreStatus . fst) $
            cBinNMUs

statusHeader :: Status -> String
statusHeader Needed = "Actually required NMUs"
statusHeader Waiting = "Already scheduled NMUs"
statusHeader Failed = "Failed builds"
statusHeader DepsMissing = "NMU seems pointless. Dependency in NEW?"
statusHeader WrongSrcVersion = "Package out of date, will be rebuilt anyways"
statusHeader PendingRemoval = "Package pending removal, don't bother"
statusHeader WrongVersion = "Packages and Wanna-Build are out of sync"
statusHeader Missing = "Packages not known to Wanna-Build. Ignoring."

actStatus :: Status -> Bool
actStatus Needed = True
actStatus _ = False

ignoreStatus :: Status -> Bool
ignoreStatus WrongSrcVersion = True
ignoreStatus _ = False

alignAt :: String -> [String] -> String
alignAt d lines = unlines (map expands rows)
  where rows = map (split (onSublist d)) lines
        widths = [map length r ++ repeat 0 | r <- rows]
        colwidths = map maximum (transpose widths)
        expand n s = s ++ replicate (n - length s) ' '
        expands [] = ""
        expands r = concat (zipWith expand colwidths (init r)) ++ last r

presentBinNMUs :: Conf -> [CBinNMU] -> IO ()
presentBinNMUs conf cBinNMUs = do
    forM_ (ordGroupBy fst cBinNMUs) $ \(s, cBinNMUs) -> do
        let binNMUs = map snd cBinNMUs

        unless (ignoreStatus s) $ do
            putCLn $ statusHeader s
            putStr $ alignAt " . "
                [ (if actStatus s then "" else "# ") ++
                  formatNMU (distribution conf) nmu
                | nmu <- sortBy (compare `on` (^. _1)) $
                         groupNMUs conf binNMUs
                ]
        when (actStatus s) $ for_ (mbPriority conf) $ \prio -> do
            putStr $ alignAt " . "
                [ formatBP prio (distribution conf) nmu
                | nmu <- sortBy (compare `on` (^. _1)) $
                         groupBPs conf binNMUs
                ]
            putStrLn ""

finalizeBinNMUs :: Conf -> [CBinNMU] -> [JBinNMU]
finalizeBinNMUs conf cBinNMUs =
    let needed = filter (actStatus . fst) cBinNMUs
        binNMUs = map snd needed
        grouped = sortBy (compare `on` (^. _1)) $ groupNMUs conf binNMUs
    in concatMap transformNMUs grouped
    where
        transformNMUs :: ([(SourceName, Version)], [Arch], [Reason]) -> [JBinNMU]
        transformNMUs (svs, as, rs) = map (\(s,v) -> transformNMU s v as (distribution conf) (rstr rs)) svs
        rstr = intercalate ", " . map formatReason
        transformNMU = JBinNMU

groupNMUs :: (Ord a, Ord b, Ord c) => Conf -> [(a, b, c)] -> [([a], [b], c)]
groupNMUs conf =
    concat .
    map (\(c,abs) ->
        map (\(bs,as) -> (as,bs,c)) $
        (if gbp then groupEqual else dontGroup) $
        map (\(a,bs) -> (bs,a)) $
        groupEqual $
        abs) .
    groupEqual .
    map (\(a,b,c) -> (c,(a,b)))
  where
    gbp = groupPkgs conf

groupBPs :: (Ord a, Ord b) => Conf -> [(a, b, c)] -> [(a, [b])]
groupBPs conf =
    groupEqual .
    map (\(a,b,c) -> (a,b))
  where
    gbp = groupPkgs conf

formatBP :: Int -> String -> ((SourceName, Version), [Arch]) -> String
formatBP prio dist (s, as) =
    printf "bp %d %s . %s . %s"
        prio
        (uncurry (printf "%s_%s") s :: String)
        (unwords (nub as))
        dist


formatNMU :: String -> ([(SourceName, Version)], [Arch], [Reason]) -> String
formatNMU dist (ss, as, d) =
    printf "nmu %s . %s . %s . -m '%s'"
        (intercalate " " $ map (uncurry (printf "%s_%s")) ss)
        (intercalate " " $ nub as)
        dist
        (intercalate ", " $ map formatReason d)

formatReason :: Reason -> String
formatReason (MissingDep d)
    = printf "%s has disappeared" (vpFull d)
formatReason (UpgradedDep base hash1 hash2)
    = printf "%s changed from %s to %s" base hash1 hash2

mkUpgradedDep :: VirtPackage -> VirtPackage -> Reason
mkUpgradedDep d1 d2 =
    case differingHashes of
        ((hash1,hash2):_) -> UpgradedDep base hash1 hash2
        [] -> UpgradedDep (vpBase d1) (dash (vpHashes d1)) (dash (vpHashes d2))
  where
    base = dash $ vpBase d1 : map fst commonHashes
    (commonHashes, differingHashes) = span (uncurry (==)) $ zip (vpHashes d1) (vpHashes d2)
    dash = intercalate "-"

-- | Data aquisition and processing
getNMUs :: Conf -> [SourceName] -> Arch -> IO [CBinNMU]
getNMUs conf rms a = do
    pkgs <- fetchArchive conf a
    wbmap <- fetchWannaBuild conf a
    let available = M.fromListWith (++) [ (vpBase v, [v])
                                        | p <- pkgs, v <- bProvides p
                                        ]
    let binNMUs = mapMaybe (needsRebuild available) pkgs
    let cBinNMUs = map (categorize available wbmap rms &&&
         (\(p,r) -> ((bSourceName p, bSrcVersion p), bArchitecture p, r))) binNMUs
    return cBinNMUs

-- Categorizing nmus

categorize :: VirtPackageMap -> WBMap -> [SourceName] -> BinNMU -> Status
categorize available wbmap rms (p,deps) =
    case M.lookup (bSourceName p) wbmap of
      Nothing -> Missing
      Just (v,bv,s)
        | s == "build-attempted"   -> Failed
        | s == "failed"            -> Failed
        | v /= bSrcVersion p       -> WrongSrcVersion
        | bSourceName p `elem` rms -> PendingRemoval
        | s == "installed" && bv /= bVersion p
                                   -> WrongVersion
        | s `elem` waiting         -> Waiting
        | any isMissing deps       -> DepsMissing
        | s == "installed"         -> Needed
        | otherwise                -> Failed
  where waiting = words "bd-uninstallable building built uploaded needs-build"

isMissing (MissingDep _) = True
isMissing _ = False


-- Calculating required binNMUs

type VirtPackageMap = M.Map VirtPackageBase [VirtPackage]

needsRebuild :: VirtPackageMap -> Binary -> Maybe BinNMU
needsRebuild available b
    | null reasons = Nothing
    | otherwise = Just (b, reasons)
  where
    reasons = mapMaybe go (bDepends b)

    go v = case M.lookup (vpBase v) available of
        Nothing                  -> Just $ MissingDep v
        Just vs | v `notElem` vs -> Just $ mkUpgradedDep v (head vs)
                | otherwise -> Nothing

-- Parsing virtual package names

parseVirtPackage :: Conf -> String -> Maybe VirtPackage
parseVirtPackage conf p = case match (regex conf) p of
    [(_:id:hashes)] -> Just (VirtPackage p id hashes)
    _ -> Nothing

-- Reading wannabuild dumps
wannaBuildDumpUrl :: Arch -> String
wannaBuildDumpUrl a = printf "https://buildd.debian.org/stats/%s-dump.txt.gz" a

type WBState = String
type WBMap = M.Map SourceName (Version, Version, WBState)


fetchWannaBuild :: Conf -> Arch -> IO WBMap
fetchWannaBuild c | sql c     = fetchWannaBuildSQL c
                  | otherwise = fetchWannaBuildHTTP c

fetchWannaBuildSQL :: Conf -> Arch -> IO WBMap
fetchWannaBuildSQL conf arch = do
    unless q $ hPutStr stderr $ printf "Querying wanna-build database for arch %s ..." arch
    unless q $ hFlush stderr
    conn <- connectPostgreSQL (B.pack "service=wanna-build")
    --- conn <- connect (ConnectInfo "localhost" 5436 "guest" "" "wanna-build")
    rows <- query conn fetchWB (arch, distribution conf)
    close conn
    unless q $ hPutStrLn stderr " done"
    return $ M.fromList $ map go rows
  where
    q = quiet conf
    go :: (String, String, Maybe Int, String) -> (SourceName, (Version, Version, WBState))
    go (src,sv,b,status) = (src, (sv, bv, map toLower status))
      where bv = case b of Nothing -> sv
                           Just n  -> sv ++ "+b" ++ show n

fetchWB = Query $ B.pack $ "\
    \SELECT    \
    \    package,    \
    \    version::text, \
    \    binary_nmu_version ,    \
    \    state  \
    \FROM    \
    \    packages_public    \
    \WHERE    \
    \    architecture = ?  \
    \    AND distribution = ? \
    \"


fetchWannaBuildHTTP :: Conf -> Arch -> IO WBMap
fetchWannaBuildHTTP conf a = do
    s <- unGZ <$> acquireFile' conf url
    case parseControl url s of
        Left pe -> error $ show pe
        Right c -> return $! M.fromList $! (map parsePara (unControl c) `using` seqList rdeepseq)
  where
    url = wannaBuildDumpUrl a

    parsePara p =
        ( reqField "package"
        , ( reqField "version"
          , reqField "version" ++ maybe "" ("+b"++) (optField "binary_nmu_version")
          , map toLower $ reqField "state"
          )
        )
      where reqField f = maybe (error $ printf "Missing field %s" f) B.unpack (fieldValue f p)
            optField f = B.unpack <$> fieldValue f p



-- Reading archive files

debianMirror :: Arch -> String
debianMirror arch = printf "https://deb.debian.org/%s" subdir
  where
    subdir = if arch `elem` portsArches then "debian-ports" else "debian"

builddMirror :: Arch -> String
builddMirror arch =
 if arch `elem` portsArches then "https://incoming.ports.debian.org/buildd"
 else "https://incoming.debian.org/debian-buildd"

packageURL :: String -> Arch -> String
packageURL suite arch = printf "%s/dists/%s/main/binary-%s/Packages.xz" mirror suite' arch
  where
    mirror = (if isPrefixOf "buildd-" suite then builddMirror else debianMirror) arch
    suite' = if arch `elem` portsArches then
               case stripPrefix "buildd-" suite of
                 Just stripped -> stripped
                 Nothing -> suite
             else suite

acquirePackagesHTTP :: Conf -> String -> Arch -> IO [Binary]
acquirePackagesHTTP conf suite arch = do
    s <- unXZ <$> acquireFile' conf url
    case parseControl url s of
        Left pe -> error $ show pe
        Right c -> return $!
            (mapMaybe parsePara (unControl c) `using` seqList rdeepseq)
  where
    url = packageURL suite arch
    parsePara :: Paragraph -> Maybe Binary
    parsePara p = if likelyInteresting then rowToBinary conf row else Nothing
      where
        reqField f = maybe (error $ printf "Missing field %s" f) B.unpack (fieldValue f p)
        optField f = B.unpack <$> fieldValue f p

        pkg = reqField "Package"
        v = reqField "Version"

        mbD = fieldValue "Depends" p
        mbP = fieldValue "Provides" p
        mbS = optField "Source"

        likelyInteresting =
            maybe False (matchTest (roughRegex conf)) mbD ||
            maybe False (matchTest (roughRegex conf)) mbP

        row = ( pkg
              , v
              , reqField "Architecture"
              , maybe pkg (fst.splitSrc) mbS
              , fromMaybe v (mbS >>= snd . splitSrc)
              , fromMaybe B.empty mbD
              , fromMaybe B.empty mbP
              )

rowToBinary :: Conf -> Row -> Maybe Binary
rowToBinary conf (pkg,v,a,s,sv,d,p) = if interesting b then Just b else Nothing
      where
        b = Binary
            pkg
            s
            a
            v
            sv
            (mapMaybe (parseVirtPackage conf) $ parseFlatRel d)
            (mapMaybe (parseVirtPackage conf) $ parseFlatRel p)


interesting :: Binary -> Bool
interesting b = not (null (bProvides b) && null (bDepends b))

splitSrc :: String -> (SourceName, Maybe Version)
splitSrc sf =
    case words sf of
        [s,'(':sv] -> (s, Just (init sv))
        [s]        -> (s, Nothing)
        _ -> error $ printf "Failed to parse source field %s" sf

fetchArchive :: Conf -> Arch -> IO [Binary]
fetchArchive c | sql c     = fetchArchiveSQL c
               | otherwise = fetchArchiveHTTP c


-- | Fetches packages for this arch, overlaying sid with buildd-sid (and
-- unreleased for ports architectures)
fetchArchiveHTTP :: Conf -> Arch -> IO [Binary]
fetchArchiveHTTP conf a = do
    let d = distribution conf
    let suites = [d, "buildd-" ++ d] ++
                 (if d == "sid" && a `elem` portsArches then ["unreleased"] else [])
    pkglists <- sequence $ map (\s -> acquirePackagesHTTP conf s a) suites
    return $ foldr (\pkgs2 pkgs1 ->
        let pkg1_names = S.fromList (map bPkgName pkgs1) in
        let pkgs2' = [ p | p <- pkgs2 , bPkgName p `S.notMember` pkg1_names ] in
        pkgs1 ++ pkgs2') [] pkglists

acquireFile' :: Conf -> String -> IO B.ByteString
acquireFile' conf url = do
    unless q $ hPutStr stderr $ printf "Fetching %s ..." url
    unless q $ hFlush stderr
    s <- acquireFile url o
    unless q $ hPutStrLn stderr $ printf " done."
    return s
  where o = offline conf
        q = o || quiet conf

type Row = (String, String, String, String, String, B.ByteString, B.ByteString)

fetchArchiveSQL :: Conf -> Arch -> IO [Binary]
fetchArchiveSQL conf arch = do
    unless q $ hPutStr stderr $ printf "Querying projectb database for arch %s ..." arch
    unless q $ hFlush stderr
    conn <- connectPostgreSQL (B.pack "service=projectb")
    -- conn <- connect (ConnectInfo "localhost" 5434 "guest" "" "projectb")
    rows <- query conn fetchBins (distribution conf, "buildd-" ++ distribution conf, arch, r, r)
    close conn
    let bins = mapMaybe (rowToBinary conf) rows
    unless q $ hPutStrLn  stderr $ " done"
    return bins
  where
    q = quiet conf
    r = ".*" ++ regexS conf ++ ".*"

fetchBins = Query $ B.pack $ "\
    \SELECT    \
    \    DISTINCT ON (package) \
    \    package,    \
    \    binaries.version::text,    \
    \    arch_string ,    \
    \    source.source,    \
    \    source.version::text,    \
    \    COALESCE(dm.value,''),    \
    \    COALESCE(pm.value,'')     \
    \FROM    \
    \    binaries    \
    \    JOIN bin_associations ON bin_associations.bin = binaries.id    \
    \    JOIN suite ON bin_associations.suite = suite.id    \
    \    JOIN architecture ON binaries.architecture = architecture.id    \
    \    JOIN source ON binaries.source = source.id    \
    \    JOIN metadata_keys dmk ON dmk.key = 'Depends'    \
    \    LEFT OUTER JOIN binaries_metadata dm ON dm.bin_id = binaries.id AND dmk.key_id = dm.key_id    \
    \    JOIN metadata_keys pmk ON pmk.key = 'Provides'    \
    \    LEFT OUTER JOIN binaries_metadata pm ON pm.bin_id = binaries.id AND pmk.key_id = pm.key_id    \
    \WHERE    \
    \    suite.codename IN (?,?)  \
    \    AND arch_string = ?    \
    \    AND (dm.value ~ ? OR pm.value ~ ? )    \
    \ORDER BY \
    \    package, binaries.version DESC \
    \"




-- Reading wnpp dumps
wnppDumpUrl :: String
wnppDumpUrl = "https://qa.debian.org/data/bts/wnpp_rm"

fetchWnppDump :: Conf -> IO [SourceName]
fetchWnppDump conf = do
    s <- acquireFile' conf url
    return $ mapMaybe parseLine $ map B.unpack $ B.lines s
  where
    url = wnppDumpUrl

    parseLine :: String -> Maybe SourceName
    parseLine l = case match wnppRegex l of
        [[_,s]] -> Just s
        _ -> Nothing

    wnppRegex :: Regex
    wnppRegex = makeRegex "^(.*): RM"


data OutputFormat = WannaBuild | JSON
    deriving (Bounded, Enum, Eq, Read, Show)

-- Option parsing
data Conf = Conf
    { distribution :: String
    , arches :: [Arch]
    , regex :: Regex
    , roughRegex :: Regex
    , regexS :: String -- A regex is not Show'able, so we need to keep the string
    , mbPriority :: Maybe Int
    , offline :: Bool
    , quiet :: Bool
    , sql :: Bool
    , groupPkgs :: Bool
    , presentProblems :: Bool
    , outputFormat :: OutputFormat
    }

mkConf :: String -> [Arch] -> String -> Maybe Int -> Bool -> Bool -> Bool -> Bool -> Bool -> OutputFormat -> Conf
mkConf d a r p =
    Conf d a (makeRegex ("^"++r++"$")) (makeRegex r) r p

parseArches :: ReadM [Arch]
parseArches = do
    s <- readerAsk
    case split (dropBlanks $ dropDelims $ oneOf ";, ") s of
        [] -> readerError "Empty list of architectures"
        arches -> case filter (not . (`elem` allArches)) arches of
            [] -> return arches
            bad -> readerError $ "Unknown architectures: " ++ intercalate ", " bad

conf :: Parser Conf
conf = mkConf
 <$> strOption (
    long "distribution" <>
    metavar "DIST" <>
    help "Distribution to produce binNMUs for" <>
    showDefault <>
    value "sid"
    )
 <*> option parseArches (
    long "arches" <>
    metavar "ARCH,ARCH,..." <>
    help "comma or space separated list of architectures" <>
    value defaultArches <>
    showDefaultWith (intercalate ", ")
    )
 <*> strOption (
    long "regex" <>
    metavar "REGEX" <>
    help "regular expression matching virtual package names, with two groups" <>
    showDefault <>
    value haskellRegex
    )
 <*> optional (option auto (
        long "priority" <>
        metavar "N" <>
        help "build priority to assign to the binNMUed packages"
    ))
 <*> switch (
    long "offline" <>
    help "do not download files (cached files must be available)"
    )
 <*> switch (
    short 'q' <>
    long "quiet" <>
    help "don't be chatty on stderr"
    )
 <*> switch (
    long "sql" <>
    help "use sql instead of downloading files. (e.g. on wuiet.debian.org)"
    )
 <*> switch (
    long "group-pkgs" <>
    help "group commands for different packages (fewer, but longer lines)"
    )
 <*> switch (
    long "present-problems" <>
    help "list all problems (changed dependencies etc.)"
    )
 <*> option auto (
    long "output-format" <>
    help "output as WannaBuild or JSON" <>
    metavar "FORMAT" <>
    showDefault <>
    value WannaBuild
    )

haskellRegex :: String
haskellRegex = "libghc-(.*)-dev-([0-9.]+)-([0-9a-f]{5})"

-- Parsing package relations with flattening
-- (this should be faster than flatRels . parseRels)
parseFlatRel :: B.ByteString -> [String]
parseFlatRel = flatRels . parseRels
  where
    flatRels :: Relations -> [String]
    flatRels = map (\(Rel (BinPkgName n) _ _) -> n) . join

    parseRels :: B.ByteString -> Relations
    parseRels s = case parseRelations s of
      Left pe ->  error $ printf "Failed to parse relations %s" (show pe)
      Right rel -> rel

-- Unpacking

unGZ :: B.ByteString -> B.ByteString
unGZ = BL.toStrict . Codec.Compression.GZip.decompress . BL.fromStrict

unXZ :: B.ByteString -> B.ByteString
unXZ = BL.toStrict . Codec.Compression.Lzma.decompress . BL.fromStrict

-- Utils

putC s = putStr  ("# " ++ s)
putCLn s = putStrLn ("# " ++ s)


-- Generic grouping algorithm

groupEqual :: Ord a => [(a,b)] -> [(a, [b])]
groupEqual = M.toList . M.fromListWith (++) . map (second (:[]))

dontGroup :: [(a,b)] -> [(a, [b])]
dontGroup xs = [(a,[b]) | (a,b) <- xs]

ordGroupBy :: Ord b => (a -> b) -> [a] -> [(b, [a])]
ordGroupBy f = groupEqual . map (f &&& id)


-- Main program

main :: IO ()
main = execParser opts >>= run
 where
  opts = info (helper <*> conf)
      ( fullDesc
     <> progDesc "Calculate Haskell packages to be binNMUed"
     <> header "binNMUs - Calculate Haskell packages to be binNMUed" )
