#!/bin/sh
# Copyright (c) 2009 Marco Túlio Gontijo e Silva <marcot@holoscopio.com>
#               2009 Joachim Breitner <nomeata@debian.org>
#               2013 Louis Bettens <louis@bettens.info>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Usage:
# pkg-haskell-checkout [PACKAGE ...]

set -e

usage () {
    cat << END
Usage: $0 [--http] PACKAGE ...

This script downloads the given packages and their debianization in the
current directory.

if --http is present, downloads the <package>/debian directory over http
instead of ssh. This works if you don't have ssh access to the Alioth server,
but http gives only a read-only access.

See http://wiki.debian.org/Alioth/SSH for information on ssh access.

If you want to reconfigure a debian directory already downloaded via http to
permit pushing via ssh, you can use:
darcs push --set-default darcs.debian.org:/darcs/pkg-haskell/<package>
END
}

darcs_get () {
    if $USE_HTTP
    then darcs get http://darcs.debian.org/$1 $2
    else darcs get darcs.debian.org:/darcs/$1 $2
    fi
}

USE_HTTP=false
until [ -z "$1" ]
do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --http)
            USE_HTTP=true
            ;;
        *)
            PACKAGES="$PACKAGES $1"
            ;;
    esac
    shift
done

if [ -z "$PACKAGES" ]
then 
   usage
   exit 1
fi

for PACKAGE in $PACKAGES
do
    if [ -e $PACKAGE-debian ]
    then
      echo "Temporary directory $PACKAGE-debian already exists, aborting"
      exit 1
    fi

    darcs_get pkg-haskell/$PACKAGE $PACKAGE-debian

    if [ ! -e $PACKAGE-debian/control -o ! -e $PACKAGE-debian/changelog ]
    then
      echo "Did not find $PACKAGE-debian/control or $PACKAGE-debian/changelog."
      echo "Is the repository in the debian/-only format?"
      exit 1
    fi

    VERSION=`dpkg-parsechangelog -l$PACKAGE-debian/changelog -c1 | grep-dctrl -n -s Version .`
    UPSTREAM=`echo $VERSION | cut -d- -f1` # this could be improved
    if echo $UPSTREAM | fgrep -q : ; then
    	UPSTREAM=`echo $UPSTREAM | cut -d: -f2-`
    fi
    TARBALL_GZ=${PACKAGE}_$UPSTREAM.orig.tar.gz
    TARBALL_BZ2=${PACKAGE}_$UPSTREAM.orig.tar.bz2
    TARBALL_XZ=${PACKAGE}_$UPSTREAM.orig.tar.xz
    WANTED_PACKAGEDIR="${PACKAGE}-${UPSTREAM}"
    # see 375138 for why this doesn't work as well as it could. Fall back to apt-get source
    # as a last resort.
    [ ! -e $TARBALL_GZ -a ! -e $TARBALL_BZ2 -a ! -e $TARBALL_XZ ] && \
       ( uscan \
        --rename \
        --force-download \
        --package $PACKAGE \
        --download \
        --watchfile $PACKAGE-debian/watch \
        --download-version $UPSTREAM \
        --upstream-version $UPSTREAM \
        --destdir . \
	--rename ||
        apt-get source $PACKAGE --tar-only )

    if [ ! -e $TARBALL_GZ -a ! -e $TARBALL_BZ2 -a ! -e $TARBALL_XZ ]
    then
      echo "Couldn't download tarball with uscan or apt-get source. See above for errors"
      exit 1
    fi

    # assumes the convention that the tarball contains one equally named directory
    # ideally, we apply the full logic as dpkg-source does
    [ -d $WANTED_PACKAGEDIR ] || mkdir $WANTED_PACKAGEDIR
    if [ -e $TARBALL_GZ ]
    then
        tar xavf $TARBALL_GZ -C $WANTED_PACKAGEDIR --strip-components 1
    else
    	if [ -e $TARBALL_XZ ]
	then
	    tar xavf $TARBALL_XZ -C $WANTED_PACKAGEDIR --strip-components 1
	else
	    tar xavf $TARBALL_BZ2 -C $WANTED_PACKAGEDIR --strip-components 1
	fi
    fi

    # debian directories shipped by upstream need to be removed
    rm -rf $WANTED_PACKAGEDIR/debian

    chmod +x $PACKAGE-debian/rules

    mv $PACKAGE-debian $WANTED_PACKAGEDIR/debian

    echo "Successfully checked out $PACKAGE in $WANTED_PACKAGEDIR"
done
