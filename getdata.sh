#!/bin/sh
mirror="http://deb.debian.org/debian"
distributions="unstable"
components="main"
#architectures="amd64 i386 alpha powerpc armel"
architectures="amd64 arm64 armel armhf i386 mips64el mipsel ppc64el s390x"

base="$mirror/dists"
test -d data/ || mkdir data/
cd data/
for distro in $distributions ; do
  for comp in $components ; do
    fname="$distro-$comp-Sources.gz"
    if ! test -L $fname
    then
      echo "Getting $fname..."
      curl -o $fname -z $fname $base/$distro/$comp/source/Sources.gz
      for arch in $architectures ; do
        fname="$distro-$comp-binary-$arch-Packages.gz"
        echo "Getting $fname..."
        curl -o $fname -z $fname $base/$distro/$comp/binary-$arch/Packages.gz
      done
    fi 
  done
done

for arch in $architectures ; do
  fname="wanna-build-dump-$arch.gz"
  if ! test -L $fname
  then
    echo "Getting $fname..."
    curl -o $fname -z $fname https://buildd.debian.org/stats/$arch-dump.txt.gz
  fi 
done

