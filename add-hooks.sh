#!/bin/sh

# Copyright (c) 2009 Marco Túlio Gontijo e Silva <marcot@holoscopio.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# USAGE:

# add-hooks.sh [PACKAGE ...]

set -e

SRC=/home/groups/pkg-haskell/tools
while [ -n "$1" ]
do
    DARCS=/darcs/pkg-haskell/$1/_darcs
    DEFAULTS=$DARCS/prefs/defaults
    if [ ! -e $DEFAULTS ]
    then
        mkdir -p -m 2775 `dirname $DEFAULTS`
        install -p -m 2664 $SRC/defaults $DEFAULTS
    fi
    TEMPLATE=$DARCS/third-party/darcs-monitor/email-template
    if [ ! -e $TEMPLATE ]
    then
        mkdir -p -m 2775 `dirname $TEMPLATE`
        install -m 2664 $SRC/email-template $TEMPLATE
    fi
    # Actually run the post-hook
    (
    cd /darcs/pkg-haskell/$1
    /home/groups/pkg-haskell/darcs-monitor --darcs-path=/home/groups/pkg-haskell/darcs --max-diff=20000 --charset=UTF-8 email pkg-haskell-commits@lists.alioth.debian.org
#    /srv/home/groups/pet/PET2/pkg-haskell/pet-darcs-helper update $1
    )
    shift
done
