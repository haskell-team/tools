#!/bin/bash

if [ "$1" = "-n" ]
then
  dry_run=1
  shift
fi

message=$1
shift
action=$1
shift

if [ -z "$message" -o -z "$action" -o "$message" = '--help' ]
then
	echo "Usage: $0 <message> '<action>' dirs..."
	echo
	echo "Runs <action> in each given directory; commits with <message> if"
	echo "<action> had an effect. Does not push, so you can fix your mess"
	echo "afterwards."
	exit
fi

while [ -n "$1" ]
do
	dir=$1
	shift

	if ! pushd "$dir"
	then
		echo "Failed to switch to \"$dir\""
		continue
	fi

	if [ ! -e changelog ]
	then
		echo "No changelog file found, skipping $dir"
		popd
		continue
	fi
	
	echo "Processing $dir"

	if darcs whatsnew -s >/dev/null
	then
		echo "Unrecorded changes in $dir, skipping:"
		darcs diff | colordiff
		popd
		continue
	fi

	# darcs pull -a -v
	eval "$action"

	if ! darcs whatsnew -s >/dev/null
	then
		echo "Action did not change $dir, skipping push"
		popd
		continue
	fi
	if [ -z "$dry_run" ]
	then
		echo "About to commit the following change:"
		darcs diff|colordiff

		lastchange="$(darcs changes --last 1 --xml-output|xpath -e 'changelog/patch/name/text()' -q)"
		if [ "$lastchange" = "$message" ]
		then
			echo "Previous commit message was the same, amending the patch"
			yes | darcs amend --all
		else
			debchange --changelog=changelog "$message"
			debcommit
		fi
	else
		echo "Would have commited the following change:"
		darcs diff|colordiff
		darcs revert --all
	fi
	popd
done
