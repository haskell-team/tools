#!/bin/bash

if [ "$1" = "-D" ]
then
	dist="$2"
	shift
	shift
fi

if [ "$1" = '--help' ]
then
	echo "Usage: $0 [dir]"
	echo
	echo "Shows the URL of the diff from this to the latest version, using"
	echo "http://hdiff.luite.com"
	echo
	echo "Unfortuntely, cgit does not support raw diffs."
	exit
fi

set -e

if [ -n "$1" ]
then
	cd $1 || ( echo "Could not chnage directory to $1" ; exit 1 )
fi

if [ ! -e changelog ]
then
	echo "No changelog file found, skipping $dir"
	popd >/dev/null
	continue
fi

if [ ! -e watch ]
then
	echo "No watch file found, skipping $dir"
	popd >/dev/null
	continue
fi

package="$(grep-dctrl -n -s Source . < control)"
old_version=`dpkg-parsechangelog -lchangelog -c1 | grep-dctrl -n -s Version .`
old_version=`echo $old_version | cut -d- -f1` # this could be improved
if echo $old_version | fgrep -q : ; then
	old_version=`echo $old_version | cut -d: -f2-`
fi

version="$(uscan --dehs --upstream-version 0 --watchfile watch --package $package --report-status|xpath -e 'dehs/upstream-version/text()' -q)"
cabal_name="$(cat watch | grep http | cut -d/ -f5)"

if [ -z "$version" ]
then
	echo "could not detect version." >&1
	exit 1
fi

if [ "$old_version" = "$version" ]
then
	exit 1
else
	echo "http://hdiff.luite.com/cgit/$cabal_name/diff/?id=$version&id2=$old_version"
fi
